<img src="<?php echo $image_desktop['url']; ?>" alt="<?php echo $image_desktop['alt']; ?>"  <?php echo (!empty($image_mob)) ? 'class="hidden-991"' : '';?> data-aos="<?php echo $fade_direction;?>">
<?php
if( !empty($image_mob) ): ?>
    <img src="<?php echo $image_mob['url']; ?>" alt="<?php echo $image_mob['alt']; ?>" class="visible-991">
<?php endif; ?>