<?php
if( !empty($image_desktop) ): ?>
    <img src="<?php echo $image_desktop['url']; ?>" <?php echo (!empty($image_mob)) ? 'class="hidden-991"' : '';?> alt="<?php echo $image_desktop['alt']; ?>">
<?php endif; ?>
<?php if( !empty($image_mob) ): ?>
    <img src="<?php echo $image_mob['url']; ?>" class="visible-991" alt="<?php echo $image_mob['alt']; ?>">
<?php endif; ?>