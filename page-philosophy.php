<?php
/** Template Name: Philosophy Page */
get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="wrapper">
        <div class="container_big">
            <div class="title">
            <?php the_content();?>
            </div>

            <div id="one" class="first_img">
                <?php
                $image_desktop = get_field('s1_image');
                $image_mob = get_field('s1_mobile_image');
                $fade_direction = 'fade-right';
                include(locate_template('template-parts/image-group-fade.php'));?>
            </div>

            <div class="philosophy_text">
                <h2 class="title_textbox">
                    <?php the_field('s1_text_title');?>
                </h2>
                <div class="textbox">
                    <?php the_field('s1_text_field');?>
                    <p><a href="<?php the_field('s1_case_study_link'); ?>">CASE STUDY</a></p>
                </div>
            </div>
            <div id="two" class="second_img">
                <?php
                $image_desktop = get_field('s2_image_top');
                $image_mob = get_field('s2_mobile_image_top');
                $fade_direction = 'fade-left';
                include(locate_template('template-parts/image-group-fade.php'));?>
            </div>
            <div id="three" class="third_img">
                <?php
                $image_desktop = get_field('s2_image_bottom');
                $image_mob = get_field('s2_mobile_image_bottom');
                $fade_direction = 'fade-right';
                include(locate_template('template-parts/image-group-fade.php'));?>
            </div>
            <div class="text2_philosophy">
                <h2 class="title_textbox">
                    <?php the_field('s2_text_title');?>
                </h2>
                <div class="textbox">
                    <?php the_field('s2_text_field');?>
                    <p><a href="<?php the_field('s2_case_study_link'); ?>">CASE STUDY</a></p>
                </div>
            </div>
            <div id="four" class="forth_img">
                <?php
                $image_desktop = get_field('s3_image_top');
                $image_mob = get_field('s3_mobile_image_top');
                $fade_direction = 'fade-right';
                include(locate_template('template-parts/image-group-fade.php'));?>
            </div>
            <div id="five" class="fifth_img">
                <?php
                $image_desktop = get_field('s3_image_bottom');
                $image_mob = get_field('s3_mobile_image_bottom');
                $fade_direction = 'fade-left';
                include(locate_template('template-parts/image-group-fade.php'));?>
            </div>
            <div class="text3_philosophy">
                <h2 class="title_textbox">
                    <?php the_field('s3_text_title');?>
                </h2>
                <div class="textbox">
                    <?php the_field('s3_text_field');?>
                    <p><a href="<?php the_field('s3_case_study_link'); ?>">CASE STUDY</a></p>
                </div>
            </div>
            <div id="six" class="six_img">
                <?php
                $image_desktop = get_field('s4_image_top');
                $image_mob = get_field('s4_mobile_image_top');
                $fade_direction = 'fade-right';
                include(locate_template('template-parts/image-group-fade.php'));?>
            </div>
            <div id="seven" class="seven_img">
                <?php
                $image_desktop = get_field('s4_image_bottom');
                $image_mob = get_field('s4_mobile_image_bottom');
                $fade_direction = 'fade-right';
                include(locate_template('template-parts/image-group-fade.php'));?>
            </div>
            <div class="text4_philosophy">
                <h2 class="title_textbox">
                    <?php the_field('s4_text_title');?>
                </h2>
                <div class="textbox">
                    <?php the_field('s4_text_field');?>
                    <p><a href="<?php the_field('s4_case_study_link'); ?>">CASE STUDY</a></p>
                </div>
            </div>
        </div>

    </div>
<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
