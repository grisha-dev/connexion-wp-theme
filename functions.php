<?php
function connexion_enqueue_assets()
{
    wp_enqueue_style('aos-css', 'https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css', null, null);
    wp_enqueue_style('immersive-slider-css', get_template_directory_uri() . '/css/immersive-slider.css');
    wp_enqueue_style('main-css', get_template_directory_uri() . '/css/main.css', null, '1.0', 'all');
    wp_enqueue_style('animate-css', get_template_directory_uri() . '/css/animate.css', null, null, 'all');

//    wp_enqueue_script('jquery-waypoints', get_template_directory_uri() . '/js/jquery.waypoints.min.js', 'jquery', null, true);
    wp_enqueue_script('moment-js', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.js', null, null, true);
    wp_enqueue_script('locales-js', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/locales.js', null, null, true);
    wp_enqueue_script('timezone-js', 'https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.3.0/moment-timezone.min.js', null, null, true);
    wp_enqueue_script('moment-timezone-utils-js', 'https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.3.0/moment-timezone-utils.min.js', null, null, true);
    wp_enqueue_script('clocks-js', get_template_directory_uri() . '/js/clocks.js', null, null, true);

    wp_enqueue_script('tether-js', 'https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js', null, null, true);

    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', 'jquery', null, true);
    wp_enqueue_script('immersive-slider-js', get_template_directory_uri() . '/js/jquery.immersive-slider.js', 'jquery', null, true);
    wp_enqueue_script('highlight-js', '//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/highlight.min.js', array('jquery'), null, true);
    wp_enqueue_script('aos-js', '//cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js', null, null, true);
    wp_enqueue_script('nicescroll-js', get_template_directory_uri() . '/js/jquery.nicescroll.js', array('jquery'), null, true);
    wp_enqueue_script('main-js', get_template_directory_uri() . '/js/main.js', array('jquery','aos-js'), null, true);

}
add_action('wp_enqueue_scripts', 'connexion_enqueue_assets');

add_theme_support( 'post-thumbnails' );
function connexion_register_post_type()
{
    $args = array(
        'label' => 'Home page Slider',
        'public' => true,
        'show_ui' => true,
        'menu_position' => 25,
        'has_archive' => false,
        'supports' => array('title'),
    );
    register_post_type('homepage-slider', $args);
    $args = array(
        'label' => 'Case Studies',
        'public' => true,
        'show_ui' => true,
        'menu_position' => 25.1,
        'has_archive' => false,
        'supports' => array('title','author'),
    );
    register_post_type('case-studies', $args);
    $args = array(
        'label' => 'Listings',
        'public' => true,
        'show_ui' => true,
        'menu_position' => 25.2,
        'has_archive' => false,
        'supports' => array('title','author'),
    );
    register_post_type('listings', $args);
}
add_action('init', 'connexion_register_post_type');

add_action("wpcf7_before_send_mail", "wpcf7_add_to_constant_contact_list");

register_nav_menus(array(
    'header-menu' => 'Header Menu',
));
require_once('class-wp-bootstrap-navwalker.php');

function wpcf7_add_to_constant_contact_list($contact_form) {
    $country_names = json_decode(file_get_contents('http://country.io/names.json'), true);
    $submission = WPCF7_Submission::get_instance();
    $posted_data = $submission->get_posted_data();

    $name = $posted_data['your-name'];
    $email = $posted_data['your-email'];
    $telephone = $posted_data['telephone'];
    $country_code = array_search  (strtolower($posted_data['country']), array_map('strtolower', $country_names));

    $request_url = 'https://api.constantcontact.com/v2/contacts?action_by=ACTION_BY_VISITOR&api_key=' . ot_get_option( 'constant_contact_api_key' );
    $access_token = ot_get_option('constant_contact_access_token');
    $headers = array(
        'Content-Type' => 'application/json',
        'Authorization' => 'Bearer ' . $access_token,

    );

    $data = array (
        'addresses' =>array( array( "address_type"=>"BUSINESS") ),
        'lists' => array( array( "id"=>ot_get_option('constant_contact_list_id') ) ),
        'email_addresses' => array( array( 'email_address' => $email ) ),
        'first_name' => $name,
        'cell_phone' => $telephone,
    );
    if ($country_code) {
        $data['addresses'][0]['country_code'] = $country_code;
    }
    $data = json_encode($data);


    $request_args = array (
        'method' => 'POST',
        'headers' => $headers,
        'body' => $data,
    );
    $response = wp_remote_request( $request_url, $request_args );


}
// Move Yoast Meta Box to bottom
function yoasttobottom() {
    return 'low';
}

add_filter( 'wpseo_metabox_prio', 'yoasttobottom');
