<?php
get_header();
 if (have_posts()) : while (have_posts()) : the_post(); ?>
     <div class="wrapper">
         <div class="container_big" style="padding-top: 0; padding-bottom: 0; margin-bottom: 0; ">
             <div class="header_all">
        <?php
         $image_desktop = get_field('cs_background_image');
         $image_mob = get_field('cs_background_image_mobile');
         if( !empty($image_desktop) ): ?>
             <div class="header_image <?php echo (!empty($image_mob)) ? 'hidden-991' : '';?>" style="background: url('<?php echo $image_desktop['url']; ?>')"></div>
         <?php endif; ?>
         <?php
         if( !empty($image_mob) ): ?>
             <div class="header_image visible-991" style="background: url('<?php echo $image_mob['url']; ?>'); display: none;"></div>
         <?php endif; ?>
             <div class="header_overlay"></div>
             <div class="title_cs"><?php the_field('cs_background_title');?></div>
             <div class="title_csl"><?php the_field('cs_background_text');?></div>
         </div>
             <div class="casestudy_text1">
                 <h1 class="title_textbox_cs">
                     <?php the_title();?>
                 </h1>
                 <div class="textbox_cs1">
                     <?php the_field('cs_top_text');?>
                 </div>
             </div>
             <div class="casestudy_image1" data-aos="fade-right">
                 <?php $image_desktop = get_field('cs_image_1'); ?>
                 <?php $image_mob = get_field('cs_image_1_mobile'); ?>
                <?php include(locate_template('template-parts/image-group.php'));?>
             </div>
             <div class="casestudy_text2">
                 <div class="textbox_cs2">
                     <?php the_field('cs_middle_text');?>
                 </div>
             </div>
             <div class="casestudy_image2" data-aos="fade-right">
                 <?php $image_desktop = get_field('cs_image_2'); ?>
                 <?php $image_mob = get_field('cs_image_2_mobile'); ?>
                 <?php include(locate_template('template-parts/image-group.php'));?>
             </div>
             <div class="casestudy_text3">
                 <div class="textbox_cs3">
                     <?php the_field('cs_bottom_text');?>
                 </div>
             </div>
             <div class="casestudy_image3" data-aos="fade-right">
                 <?php $image_desktop = get_field('cs_image_3'); ?>
                 <?php $image_mob = get_field('cs_image_3_mobile'); ?>
                 <?php include(locate_template('template-parts/image-group.php'));?>
             </div>
         </div>
     </div>
 <?php endwhile;
 else : ?>
     <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
 <?php endif; ?>
<?php get_footer(); ?>
