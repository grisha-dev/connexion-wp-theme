jQuery(document).ready(function($){
    $("body").niceScroll({
        zindex: 1500,
        scrollspeed: 110, // scrolling speed
        mousescrollstep: 20, // scrolling speed with mouse wheel (pixel)
        enablemousewheel: 1000, // nicescroll can manage mouse wheel events
        enablekeyboard: true, // nicescroll can manage keyboard events
        smoothscroll: true, // scroll with ease movement
        sensitiverail: true, // click on rail make a scroll
        oneaxismousemode: "false",
        railvalign: false, // alignment of horizontal rail
        grabcursorenabled: true,
        nativeparentscrolling: true // detect bottom of content and let parent to scroll, as native scroll does

    });


    if($('.preloader').length) {
        $(window).load(function () {
            setTimeout(function () {
                $('.preloader').fadeOut('slow', function () {
                    $(this).attr("style", "display: none !important");
                });
            }, 2000);
        });
    }

    if($('#immersive_slider').length) {
        $("#immersive_slider").immersive_slider({
            container: ".main",
        });
    }

    $('.front-page-navbar-toggler').click(function () {
        $('.navbar').toggleClass("black", 250, "easeOutSine");
    });

    if($('#pagination').length) {
        var ua = navigator.userAgent;
        window.onload = function () {
            if (ua.search(/Safari/i) > 0 && ua.search(/iPad/i) > 0 ) {
                document.getElementById('pagination').style.bottom = '70px';
            }
            if (ua.search(/Chrome/) > 0) {
                document.getElementById('pagination').style.bottom = '0px';
            }
            if (ua.search(/CriOS/) > 0) {
                document.getElementById('pagination').style.bottom = '0px';
            }
        }
    }

    if(window.location.href.indexOf("listings") > -1) {
        $('ul#menu-header-menu .menu-item-object-listings').addClass('active');
    }
});
jQuery(function($){
    $(window).load(function () {
        AOS.init({
            duration: 700,
            disable: window.innerWidth < 768
        });
    });


});
