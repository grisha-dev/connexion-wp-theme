//First image animation
var waypoint = new Waypoint({

    element: document.getElementById('one'),
    handler: function(dir) {
        if(window.innerWidth > 992) {
            $('#one').addClass('animated').addClass('fadeInLeft');
        }
    },
    offset: '75%'
})

var waypoint = new Waypoint({

    element: document.getElementById('one'),
    handler: function(dir) {
        if(window.innerWidth > 992) {
            if (dir === 'down') $('#one').addClass('fadeOutLeft').removeClass('fadeInLeft');
            else $('#one').removeClass('fadeOutLeft').addClass('fadeInLeft');
        }
    },
    offset: '-350px'
})

//Second image animation
var waypoint = new Waypoint({
    mobile: false,
    element: document.getElementById('two'),
    handler: function(dir) {
        if(window.innerWidth > 992) {
            $('#two').addClass('animated').addClass('fadeInRight');
        }
    },
    offset: '75%'
})

var waypoint = new Waypoint({
    mobile: false,
    element: document.getElementById('two'),
    handler: function(dir) {
        if(window.innerWidth > 992) {
            if (dir === 'down') $('#two').addClass('fadeOutRight').removeClass('fadeInRight');
            else $('#two').removeClass('fadeOutRight').addClass('fadeInRight');
        }
    },
    offset: '-350px'
})

//Three image animation
var waypoint = new Waypoint({
    mobile: false,
    element: document.getElementById('three'),
    handler: function(dir) {
        if(window.innerWidth > 992) {
            $('#three').addClass('animated').addClass('fadeInLeft');
        }
    },
    offset: '75%'
})

var waypoint = new Waypoint({
    element: document.getElementById('three'),
    handler: function(dir) {
        if(window.innerWidth > 992) {
            if (dir === 'down') $('#three').addClass('fadeOutLeft').removeClass('fadeInLeft');
            else $('#three').removeClass('fadeOutLeft').addClass('fadeInLeft');
        }
    },
    offset: '-230px'
})

//Four image animation
var waypoint = new Waypoint({
    element: document.getElementById('four'),
    handler: function(dir) {
        if(window.innerWidth > 992) {
            $('#four').addClass('animated').addClass('fadeInLeft');
        }
    },
    offset: '75%'
})

var waypoint = new Waypoint({
    element: document.getElementById('four'),
    handler: function(dir) {
        if(window.innerWidth > 992) {
            if (dir === 'down') $('#four').addClass('fadeOutLeft').removeClass('fadeInLeft');
            else $('#four').removeClass('fadeOutLeft').addClass('fadeInLeft');
        }
    },
    offset: '-350px'
})

//Five image animation
var waypoint = new Waypoint({
    element: document.getElementById('five'),
    handler: function(dir) {
        if(window.innerWidth > 992) {
            $('#five').addClass('animated').addClass('fadeInRight');
        }
    },
    offset: '75%'
})

var waypoint = new Waypoint({
    element: document.getElementById('five'),
    handler: function(dir) {
        if(window.innerWidth > 992) {
            if (dir === 'down') $('#five').addClass('fadeOutRight').removeClass('fadeInRight');
            else $('#five').removeClass('fadeOutRight').addClass('fadeInRight');
        }
    },
    offset: '-230px'
})

//six image animation
var waypoint = new Waypoint({
    element: document.getElementById('six'),
    handler: function(dir) {
        if(window.innerWidth > 992) {
            $('#six').addClass('animated').addClass('fadeInLeft');
        }
    },
    offset: '75%'
})

//Seven image animation
var waypoint = new Waypoint({
    element: document.getElementById('seven'),
    handler: function(dir) {
        if(window.innerWidth > 992) {
            $('#seven').addClass('fadeInLeft');
        }
    },
    offset: '75%'
})