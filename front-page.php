<?php get_header(); ?>
    <div class="wrapper1">
        <div class="main">
            <div class="page_container">
                <?php $args = array(
                    'post_type' => 'homepage-slider',
                    'posts_per_page'	=>  -1,
                    'post_status'       => 'publish',
                );
                $query = new WP_Query( $args );
                $counter = 1;
                if ( $query->have_posts() ) :?>
                    <div id="immersive_slider" style="background-image: url('<?php echo get_template_directory_uri() . '/img/slider_overlay.png'?>')">
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <?php $slide_image = get_field('home_slider_image'); ?>
                            <?php $slide_image_mob = get_field('home_slider_image_mobile'); ?>
                            <div class="slide" data-image="<?php echo $slide_image['url']?>" <?php echo (!empty($slide_image_mob)) ? 'data-image-mob="' . $slide_image_mob['url'] . '"' : '';?>>
                                <div class="content">
                                    <h2 style="font-family: 'Perpetua boldital', 'Perpetua', sans-serif; text-align: center"><?php the_field('home_slider_quote'); ?></h2>
                                    <h6 style="text-align: center; color: white; opacity: 0.3; font-size: 10px"><?php the_field('home_slider_quote_author'); ?></h6>
                                </div>
                            </div>
                            <style>
                                .is-pagination  a.page-<?php echo $counter?>:before {
                                    content: "<?php the_field('home_slider_address');?>";
                                    display: flex;
                                }
                            </style>
                            <?php $counter ++ ;?>
                        <?php endwhile;?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
