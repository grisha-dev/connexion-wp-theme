<?php get_header(); ?>

            <section class="error-404 not-found">
                <h1 class="title_textbox text-center">Oops! That page can&rsquo;t be found.</h1>
            </section><!-- .error-404 -->

<?php get_footer();