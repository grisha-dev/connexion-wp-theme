<!DOCTYPE html>
<html lang="en" <?php echo (is_front_page() || is_page_template('page-contacts.php')) ? 'style="height:100%;"' : ''?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?php if(is_page_template('page-philosophy.php')): ?>
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="-1" />
    <?php endif; ?>
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class();?>>
<?php if(is_front_page()): ?>
    <?php if ( function_exists( 'ot_get_option' ) ) :
        $splash_background_image = ot_get_option( 'splash_background_image' );
        $splash_logo = ot_get_option('splash_logo');
        $splash_background_image_mobile = ot_get_option( 'splash_background_image_mobile' );
        $splash_logo_mobile = ot_get_option('splash_logo_mobile'); ?>
        <div class="preloader hidden-640" style="background-image: url('<?php echo $splash_background_image; ?>')"><img src="<?php echo $splash_logo;?>" class="logodt" alt="/"><img src="<?php echo $splash_logo_mobile;?>" class="logomob" alt="/"></div>
        <div class="preloader visible-640" style="background-image: url('<?php echo $splash_background_image_mobile; ?>'); display: none;"><img src="<?php echo $splash_logo;?>" class="logodt" alt="/"><img src="<?php echo $splash_logo_mobile;?>" class="logomob" alt="/"></div>
    <?php endif;?>
<?php endif;?>
<!--Menu-->
<nav class="navbar navbar-toggleable-md fixed-top <?php echo (is_front_page()) ?  'bg-main-transparent' : 'bg-main-white'; ?>">
    <button class="navbar-toggler navbar-toggler-right collapsed <?php echo (is_front_page()) ?  'front-page-navbar-toggler' : ''; ?>" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <div class="burger"></div>
        <div class="burger"></div>
        <div class="burger"></div>
    </button>
    <?php if ( function_exists( 'ot_get_option' ) ) :
        $other_pages_logo = ot_get_option( 'other_pages_logo' );
        $home_page_logo = ot_get_option('home_page_logo'); ?>
    <a id="ggg" class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php echo $other_pages_logo;?>" class="grey" alt="/"><img src="<?php echo $home_page_logo;?>" class="white" alt="/" ></a>
    
    <?php endif;
    $title = '';
    if ( get_query_var('pagename')) :
        $title = get_query_var('pagename');?>
    <?php elseif (get_post_type() == 'case-studies' || get_post_type() == 'listings'):
        $title = 'listings';
    endif;?>
    <?php if($title): ?>
        <div class="linke"><?php echo $title;?></div>
    <?php endif;?>


    <?php
    wp_nav_menu( array(
            'theme_location'    => 'header-menu',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse justify-content-end',
            'container_id'      => 'navbarCollapse',
            'menu_class'        => 'navbar-nav',
            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
            'walker'            => new WP_Bootstrap_Navwalker())
    );
    ?>
</nav>