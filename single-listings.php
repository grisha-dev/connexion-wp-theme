<?php
get_header();
if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="wrapper">
        <!-- Default container for page's content max-with 1366px-->
        <div class="container_big" style="padding-top: 0; margin-bottom: 0; padding-bottom: 0">
            <!-- First block header -->
            <div class="header_gallery">
                <?php
                $image_desktop = get_field('l_background_image');
                $image_mob = get_field('l_background_image_mobile');
                if( !empty($image_desktop) ): ?>
                    <div class="header_image_study <?php echo (!empty($image_mob)) ? 'hidden-991' : '';?>" style="background-image: url('<?php echo $image_desktop['url']; ?>')"></div>
                <?php endif; ?>
                <?php
                if( !empty($image_mob) ): ?>
                    <div class="header_image_study visible-991" style="background-image: url('<?php echo $image_mob['url']; ?>'); display: none"></div>
                <?php endif; ?>
                <div class="header_overlay_gallery"></div>
                <h1 class="header_title_gallery"><?php the_title();?></h1>
                <?php previous_post_link($format = '&laquo; %link', $link = 'Previous') ?>
                <?php next_post_link($format = '&laquo; %link', $link = 'Next') ?>
            </div>


            <!-- Block content (This block can be changed)-->
            <div class="block_header_gallery">
                <div class="title_textbox_gallery">
                    <span><?php the_field('l_address');?></span><br />
                    <?php
                    $info_string = '';
                    if(get_field('l_price')) {
                        $info_string .= get_field('l_price') . ' | ';
                    }
                    if(get_field('l_beds_count')) {
                        $info_string .= get_field('l_beds_count') . ' | ';
                    }
                    if(get_field('l_baths')) {
                        $info_string .= get_field('l_baths') . ' | ';
                    }
                    if(get_field('l_square_feet')) {
                        $info_string .= get_field('l_square_feet');
                    }
                    ?>
                    <?php if($info_string):?>
                        <span class="two"><?php echo $info_string;?></span>
                    <?php endif;?>
                </div>
                <div class="textbox">
                    <?php the_field('l_description');?>
                </div>
            </div>

            <!-- Block with children-->
            <div class="ntnblock">
                <?php
                $image_desktop = get_field('l_image_1');
                $image_mob = get_field('l_image_1_mobile');
                $fade_direction = 'fade-right';
                if( !empty($image_desktop) ): ?>
                <!-- First block content (This block can be changed) -->
                    <div class="wrapper_element" style="height: <?php the_field('block_height_1');?>px">
                        <div class="image1_gallery">
                            <?php include(locate_template('template-parts/image-group-fade.php'));?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if(!empty(get_field('l_image_2')) || !empty(get_field('l_image_3'))):?>
                <!-- Second block content (This block can be changed) -->
                <div class="wrapper_element" style="height: <?php the_field('block_height_2');?>px">
                    <?php
                    $image_desktop = get_field('l_image_2');
                    $image_mob = get_field('l_image_2_mobile');
                    $fade_direction = 'fade-right';
                    if( !empty($image_desktop) ): ?>
                        <div class="image2_gallery">
                            <?php include(locate_template('template-parts/image-group-fade.php'));?>
                        </div>
                    <?php endif; ?>
                    <?php
                    $image_desktop = get_field('l_image_3');
                    $image_mob = get_field('l_image_3_mobile');
                    $fade_direction = 'fade-left';
                    if( !empty($image_desktop) ): ?>
                    <div class="image3_gallery">
                        <?php include(locate_template('template-parts/image-group-fade.php'));?>
                    </div>
                    <?php endif; ?>
                    <?php if(get_field('l_text_field_1')): ?>
                        <div class="block_text1_gallery">
                            <h2 class="title_textbox">
                                <?php the_field('l_text_title_1');?>
                            </h2>
                            <div class="textbox">
                                <?php the_field('l_text_field_1');?>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
                <?php endif; ?>
                <?php if(!empty(get_field('l_image_4')) || !empty(get_field('l_image_5'))):?>
                <!-- Third block content (This block can be changed) -->
                <div class="wrapper_element" style="height: <?php the_field('block_height_3');?>px">
                    <?php
                    $image_desktop = get_field('l_image_4');
                    $image_mob = get_field('l_image_4_mobile');
                    $fade_direction = 'fade-right';
                    if( !empty($image_desktop) ): ?>
                        <div class="image4_gallery">
                            <?php include(locate_template('template-parts/image-group-fade.php'));?>
                        </div>
                    <?php endif; ?>
                    <?php if(get_field('l_text_field_2')): ?>
                        <div class="block_text2_gallery">
                            <h2 class="title_textbox">
                                <?php the_field('l_text_title_2');?>
                            </h2>
                            <div class="textbox">
                                <?php the_field('l_text_field_2');?>
                            </div>
                        </div>
                    <?php endif;?>
                    <?php
                    $image_desktop = get_field('l_image_5');
                    $image_mob = get_field('l_image_5_mobile');
                    $fade_direction = 'fade-left';
                    if( !empty($image_desktop) ): ?>
                        <div class="image5_gallery">
                            <?php include(locate_template('template-parts/image-group-fade.php'));?>
                        </div>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
                <?php
                $image_desktop = get_field('l_image_6');
                $image_mob = get_field('l_image_6_mobile');
                $fade_direction = 'fade-right';
                if( !empty($image_desktop) ): ?>
                <!-- Fourth block content (This block can be changed) -->
                <div class="wrapper_element" style="height: <?php the_field('block_height_4');?>px">
                    <div class="image6_gallery">
                        <?php include(locate_template('template-parts/image-group-fade.php'));?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endwhile;
else : ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>
